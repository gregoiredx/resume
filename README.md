Generates pdf résumé from html template.

Latest version is available at https://gregoiredx.gitlab.io/resume/cv-fr.pdf.

Designs are forked from https://github.com/mnjul/html-resume.

