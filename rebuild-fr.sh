#!/usr/bin/env bash

docker run --rm -u $(id -u ${USER}):$(id -g ${USER}) -v "$PWD:/code" -w /code --entrypoint "watch" resume npm run rebuild-fr
