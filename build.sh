#!/usr/bin/env bash

docker build -t resume .
docker run --rm -u $(id -u ${USER}):$(id -g ${USER}) -v "$PWD:/code" -w /code --entrypoint "npm" resume install
docker run --rm -u $(id -u ${USER}):$(id -g ${USER}) -v "$PWD:/code" -w /code --entrypoint "npm" resume start
